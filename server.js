const app = require('express')();
const mongoosastic = require('mongoosastic');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.connect('mongodb://mongo:27017/dogs');

const CatSchema = new Schema({
    name: String
}).plugin(mongoosastic, {
    index: "docker",
    host: "elasticsearch",
    port: 9200
});

const Cat = mongoose.model("Cat", CatSchema);

app.listen(3000);

app.get("/", (req, res) => res.send('welcome !'));
app.get("/about", (req, res) => res.send('about'));
app.get("/contact", (req, res) => res.send('PAOK'));
app.get("/create", (req, res) => {
    
    const documents = [];
    let num = 0;

    const names = ["PAOK","Vasilis M.","Karydakis","Nikolhs","George","Vasilis T.","Swkraths","ARIS"];

    for(let i = 0; i<1; i++){
        for(let i = 0; i<1000; i++){
            documents.push({ name: names[Math.floor((Math.random() * 7) + 1)] });
            num++;
        }
        Cat.insertMany(documents);
    }
    
    return res.send('Softex: miaouuuu '+num+' !!!');
});
app.get("/search", (req, res) => {
    Cat.search({match_all: {}}, {
        "from": 0,
        "size": 0,
        "aggs": {
            "group_by_name": {
                "terms": {
                    "field": "name.keyword"
                }
            }
        }
    }, function(err, results) {
        if(err) return res.json({error: err});
        res.json(results);
    });
});
app.get("/sync", (req, res) => {
    let stream = Cat.synchronize();

    stream.on('close', function(){
        res.json({message: 'ok'});
    });
});

app.get("/mongo", (req, res) => {
    Cat.aggregate([
        {
            $group : {
                _id : "$name",
                count: { $sum: 1 }
            }
        }
    ], function(err, results) {
        if(err) return res.json({error: err});
        res.json(results);
    });
});